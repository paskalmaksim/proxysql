FROM debian:jessie

RUN apt-get update && \
    apt-get install -y wget mysql-client && \
    wget https://github.com/sysown/proxysql/releases/download/v1.4.12/proxysql_1.4.12-dbg-debian8_amd64.deb -O /opt/proxysql.deb && \
    dpkg -i /opt/proxysql.deb && \
    rm -f /opt/proxysql.deb && \
    rm -rf /var/lib/apt/lists/* \
    echo "mysql -u admin -padmin -h 127.0.0.1 -P6032" > /usr/local/bin/proxysql-console \
    chmod +x /usr/local/bin/proxysql-console

CMD ["proxysql", "-f"]
